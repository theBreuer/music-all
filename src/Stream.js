import React, { Component } from 'react';
import { connect } from 'react-redux'

import { controllerAction, controlActions } from './actionHelper'
import { types } from './appRedux'

import './Stream.css'

class Stream extends Component {

  onStreamClick = () => {
    const {id, clientId} = this.props
    this.props.onStreamClick(id, clientId)
  }

  render() {
    const { image } = this.props

    return (
      <div className="streamInfo" onClick={this.onStreamClick}> 
        <img className="trackImage" src={image} alt="track art" />
        {/* <div className="trackInformation">{name} - {artist} </div> */}
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onStreamClick: (streamId, clientId) => {
      dispatch(
      controllerAction(clientId, controlActions.set_stream, streamId)
      )
      dispatch({
        type: types.HIDE_STREAM_MODAL
      })
    }
  }
}

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(Stream)
