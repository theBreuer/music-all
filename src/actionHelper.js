import { types } from './appRedux'

export const controlActions = {
  play: "play",
  pause: "pause",
  previous: "previous",
  next: "next",
  volume_up: "volume_up",
  volume_down: "volume_down",
  mute: "mute",
  unmute: "unmute",
  set_stream: "set_stream",
  set_playlist: "load",
  off: "off"
}

export function startUp(){
    return function(dispatch, getState){
      const mqtt = require('mqtt')
      const mqttClient = mqtt.connect('mqtt://haserver.home:1884', {
        username: "internal",
        password:"turnoffthelights"
      })

      dispatch({
        type: types.UPDATE_MQTT_CLIENT,
        mqttClient: mqttClient
      })

      mqttClient.on("connect", function(){
        dispatch(mqttConnected(mqttClient));
      })

      mqttClient.on("message", function(topic, payload) {
        if (topic === "music/server/state"){
          dispatch(mqttMessage(topic, payload))
        }
      })
    }
  }

export function mqttConnected(mqttClient) {
  return function(dispatch, getState){
    mqttClient.subscribe("music/server/#")
    mqttClient.publish('music/server/refresh', '')
  }
}

export function mqttMessage(topic, payload){
  return function(dispatch, getState){
    const pl = JSON.parse(payload.toString());
    dispatch({
        type: types.UPDATE_STATE,
        state: pl
    })
  }
}

export function controllerAction(clientId, action, value) {
  return function(dispatch, getState){
    const volumeAdjustment = 5
    const state = getState()
    const mqttClient = state.mqttClient

    //TODO don't allow volume <0 >100

    switch (action) {
      case controlActions.volume_up:
        action = "volume"
        value += volumeAdjustment
        break;
      case controlActions.volume_down:
        action = "volume"
        value -= volumeAdjustment
        break;
      default:
    }

    const topic = "music/server/control/" + clientId + "/" + action
    console.log("Topic: " + topic)
    mqttClient.publish(topic, String(value))
  }
}
