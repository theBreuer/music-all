import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { reducer } from './appRedux'

import './index.css';

import App from './App';

export default function configureStore(initialState) {
    return createStore(
        reducer,
        applyMiddleware(thunk)
    );
}

const store = configureStore();

const AppWithStore = (
  <Provider store={store}>
    <App />
  </Provider>
)

ReactDOM.render(AppWithStore, document.getElementById('root'))
