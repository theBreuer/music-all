import React, { Component } from 'react';
import { connect } from 'react-redux'

import { controllerAction, controlActions } from './actionHelper'
import { types } from './appRedux'

import './Playlist.css'

class Playlist extends Component {

  onPlaylistClick = () => {
    const {uri, clientId} = this.props
    this.props.onPlaylistClick(clientId, uri)
  }

  render() {
    const { name, image } = this.props

    return (
      <div className="streamInfo" onClick={this.onPlaylistClick}> 
        <img className="trackImage" src={image} alt="track art" />
        <div className="trackInformation">{name}</div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onPlaylistClick: (clientId, playlistId) => {
      dispatch(controllerAction(clientId, controlActions.set_playlist, playlistId))
      dispatch({
        type: types.HIDE_STREAM_MODAL
      })
    }
  }
}

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(Playlist)
