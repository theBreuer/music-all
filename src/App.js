import React, { Component } from 'react';
import { connect } from 'react-redux'

import Controller from './Controller'
import StreamModal from './StreamModal'

import { startUp } from './actionHelper.js'

import './App.css';

class App extends Component {
  componentWillMount() {
    this.props.startUp()
  }

  render() {
    const {clients, streams, playlists, streamModal} = this.props

    return (
      <div className="app">
        <div className="controllers">
          {clients && clients.map(function(client, index){
              return <Controller key={index} {...client} />
            })}
        </div>

        <StreamModal
          streams={streams}
          playlists={playlists}
          streamModal={streamModal} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  clients: state.clients,
  streams: state.streams,
  playlists: state.playlists,
  streamModal: state.streamModal
})

const mapDispatchToProps = dispatch => {
  return {
    startUp: () => dispatch(startUp())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
